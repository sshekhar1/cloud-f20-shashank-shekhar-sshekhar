"""
Python list model
"""
from datetime import date
from Model import Model

class model(Model):
    def __init__(self):
        self.guestentries = []

    def select(self):
        """
        Returns guestentries list of lists
        Each list in guestentries contains: name, Description, Street_Address, Type_of_service, Phone_number, Hours_of_Operation, Reviews
        :return: List of lists
        """
        return self.guestentries

    def insert(self, name, Description, Street_Address, Type_of_service, Phone_number, Hours_of_Operation, Reviews):
        """
        Appends a new list of values representing new message into guestentries
        :param name: String
        :param Description: String
        :param Street_Address: String
        :param Type_of_service: String
        :param Phone_number: int
        :param Hours_of_Operation: int
        :param Reviews: String
        :return: True

        """
        params = [name, Description, Street_Address, Type_of_service, Phone_number, Hours_of_Operation, Reviews, date.today()]
        self.guestentries.append(params)
        return True

