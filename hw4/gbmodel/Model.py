class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, Description, Street_Address, Type_of_service, Phone_number, Hours_of_Operation, Reviews):
        """
        Inserts entry into database
        :param name: String
        :param Description: String
        :param Street_Address: String
        :param Type_of_service: String
        :param Phone_number: int
        :param Hours_of_Operation: int
        :param Reviews: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass

