from .Model import Model
from datetime import datetime
from google.cloud import datastore

def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.
    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]
    This returns:
        [ name, Description, Street_Address, Type_of_service, Phone_number, Hours_of_Operation, Reviews ]
    where name, Description, Street_Address, Type_of_service and Reviews are Python strings
    and where date is a Python datetime
    """
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['name'],entity['Description'],entity['Street_Address'],entity['Type_of_service'],entity['Phone_number'],entity['Hours_of_Operation'],entity['Reviews']]

class model(Model):
    def __init__(self):
        self.client = datastore.Client('cloud-f20-shashank-sh-sshekhar')

    def select(self):
        query = self.client.query(kind = 'Reviewhw')
        entities = list(map(from_datastore,query.fetch()))
        return entities

    def insert(self,name, Description, Street_Address, Type_of_service, Phone_number, Hours_of_Operation, Reviews):
        key = self.client.key('Reviewhw')
        rev = datastore.Entity(key)
        rev.update( {
            'name': name,
            'Description' : Description,
            'Street_Address' : Street_Address,
            'Type_of_service' : Type_of_service,
            'Phone_number' : Phone_number,
            'Hours_of_Operation' : Hours_of_Operation,
            'Reviews' : Reviews,
            'date' : datetime.today()
            })
        self.client.put(rev)
        return True
