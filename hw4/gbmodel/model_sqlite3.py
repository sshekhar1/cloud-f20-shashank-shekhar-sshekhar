from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from charity_service")
        except sqlite3.OperationalError:
            cursor.execute("create table charity_service (name text, Description text, Street_Address text, Type_of_service text,Phone_number int, Hours_of_Operation int, Reviews text, date date)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, Type of service, date, Reviews
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM charity_service")
        return cursor.fetchall()

    def insert(self, name, Description, Street_Address, Type_of_service, Phone_number, Hours_of_Operation, Reviews):
        """
        Inserts entry into database
        :param name: String
        :param Description: String
        :param Street_Address: String
        :param Type_of_service: String
        :param Phone_number: int
        :param Hours_of_Operation: int
        :param Reviews: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'name':name, 'Description':Description, 'Street_Address':Street_Address, 'Type_of_service':Type_of_service, 'Phone_number':Phone_number, 'Hours_of_Operation':Hours_of_Operation,
        'Reviews':Reviews, 'date':date.today()}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into charity_service (name, Description, Street_Address, Type_of_service, Phone_number, Hours_of_Operation, Reviews, date)" "VALUES                               			(:name, :Description, :Street_Address, :Type_of_service, :Phone_number, :Hours_of_Operation, :Reviews, :date)", params)
        connection.commit()
        cursor.close()
        return True

