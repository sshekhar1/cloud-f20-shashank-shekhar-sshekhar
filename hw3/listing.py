"""
list of fields
"""
from flask import render_template
from flask.views import MethodView
import gbmodel

class Listing(MethodView):
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(name=row[0], Description=row[1], Street_Address=row[2], Type_of_service=row[3], Phone_number=row[4], Hours_of_Operation=row[5], Reviews=row[6], date=row[7] ) for row in model.select()]

        return render_template('listing.html',entries=entries)

