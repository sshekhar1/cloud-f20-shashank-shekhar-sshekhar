from flask import Flask, redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Submit(MethodView):
   
    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = gbmodel.get_model()
        model.insert(request.form['name'], request.form['Description'], request.form['Street_Address'], request.form['Type_of_service'],
        request.form['Phone_number'], request.form['Hours_of_Operation'], request.form['Reviews'])
        return redirect(url_for('form'))

