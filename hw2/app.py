"""
Web Application for listing charities and social services
"""
import flask
from flask.views import MethodView
from index import Index
from form import Form
from listing import Listing
from submit import Submit


app = flask.Flask(__name__)       # our Flask app

app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=['GET'])

app.add_url_rule('/listing/',
                 view_func=Listing.as_view('listing'),
                 methods=['GET'])

app.add_url_rule('/form/',
        view_func=Form.as_view('form'))

app.add_url_rule('/submit/',
        view_func=Submit.as_view('submit'),
        methods=['POST'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)

